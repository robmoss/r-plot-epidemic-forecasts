## Plot epidemic forecasts produced by epifx

This package provides functions to load and plot the forecasting outputs
produced by the Python package
[epifx](https://bitbucket.org/robmoss/epidemic-forecasting-for-python), by way
of the [ggplot2](http://ggplot2.org/) plotting package for
[R](https://www.r-project.org/).

## License

This work is made available under the BSD 2-Clause license (see
`epifxplot/LICENSE`).

## Requirements

This package requires [R](https://www.r-project.org/) 2.15 or newer, and the
following packages:

- [ggplot2](http://ggplot2.org/) >= 2.0.0
- [scales](https://cran.r-project.org/web/packages/scales/) >= 0.3.0
- [rhdf5](http://bioconductor.org/packages/rhdf5/) >= 2.14.0
- [themergm](https://bitbucket.org/robmoss/ggplot2-theme-rgm) >= 1.6

The [Cairo](http://cran.r-project.org/web/packages/Cairo/) package is also
suggested for printing plots to disk (e.g., as PDF or PNG files).

## Usage

    library(epifxplot)

    # Identify an epifx output file.
    hdf5_file <- '...'

    # Plot the forecasts generated at each forecasting date.
    p1 <- plot_forecasts(hdf5_file, obs=TRUE)

    # Plot the peak timing predictions.
    p2 <- plot_peak_timing(hdf5_file, obs=TRUE)

## Install

Clone the repository, `cd` into the root directory, and run:

    make install

This will install the package by running `R CMD INSTALL epifxplot`.
