PKG = epifxplot

R_FILES := $(wildcard $(PKG)/R/*.R)

# Check whether roxygen2 is installed (return value is TRUE or FALSE).
ROXYGEN2 := $(shell Rscript --vanilla --quiet \
              -e 'cat(suppressWarnings(require(roxygen2, quietly=TRUE)))')

default: check

check:
	@_R_CHECK_FORCE_SUGGESTS_=0 R CMD check $(PKG)

doc:
ifeq ($(ROXYGEN2),TRUE)
	@R --vanilla --quiet -e "library(roxygen2); roxygenise('"$(PKG)"')"
else
	@echo Not updating man files, roxygen2 not installed
endif

pdf: $(PKG).pdf

$(PKG).pdf: doc $(R_FILES)
	@R CMD Rd2pdf --force --no-preview $(PKG)

install: pdf
	@R CMD INSTALL $(PKG)

distclean:
	@rm -rf $(PKG).Rcheck $(PKG).pdf

.PHONY: default check doc install distclean
